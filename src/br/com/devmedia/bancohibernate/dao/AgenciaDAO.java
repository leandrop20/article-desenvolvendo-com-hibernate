package br.com.devmedia.bancohibernate.dao;

import br.com.devmedia.bancohibernate.dao.commons.CrudDAO;
import br.com.devmedia.bancohibernate.model.Agencia;

public interface AgenciaDAO extends CrudDAO<Agencia> {

	
	
}