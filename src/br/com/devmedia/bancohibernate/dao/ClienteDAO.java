package br.com.devmedia.bancohibernate.dao;

import java.util.List;

import br.com.devmedia.bancohibernate.dao.commons.CrudDAO;
import br.com.devmedia.bancohibernate.exception.DAOException;
import br.com.devmedia.bancohibernate.model.Cliente;

public interface ClienteDAO extends CrudDAO<Cliente> {

	public List<Cliente> findAllClientesDividaChequeEspecial() throws DAOException;
	public List<Cliente> findAllClientesVips() throws DAOException;
	
}