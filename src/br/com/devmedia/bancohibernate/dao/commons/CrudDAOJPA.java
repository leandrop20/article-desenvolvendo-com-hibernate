package br.com.devmedia.bancohibernate.dao.commons;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.devmedia.bancohibernate.exception.DAOException;

public class CrudDAOJPA<T> implements CrudDAO<T> {

	private Class<T> classEntity;
	protected static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit");

	@SuppressWarnings("unchecked")
	public CrudDAOJPA() {
		this.classEntity = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "from " + classEntity.getSimpleName();
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public T findById(Serializable id) throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			return em.find(classEntity, id);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void addEntity(T entity) throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void updateEntity(T entity) throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void removeEntity(T entity) throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(em.contains(entity) ? entity : em.merge(entity));
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}