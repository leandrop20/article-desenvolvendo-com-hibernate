package br.com.devmedia.bancohibernate.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import br.com.devmedia.bancohibernate.dao.ContaDAO;
import br.com.devmedia.bancohibernate.dao.impl.AgenciaDAOImpl;
import br.com.devmedia.bancohibernate.dao.impl.ClienteDAOImpl;
import br.com.devmedia.bancohibernate.dao.impl.ContaDAOImpl;
import br.com.devmedia.bancohibernate.exception.BusinessException;
import br.com.devmedia.bancohibernate.exception.DAOException;
import br.com.devmedia.bancohibernate.model.Agencia;
import br.com.devmedia.bancohibernate.model.Cliente;
import br.com.devmedia.bancohibernate.model.Conta;
import br.com.devmedia.bancohibernate.model.Corrente;
import br.com.devmedia.bancohibernate.model.Poupanca;

public class Test {

	public static void main(String[] args) throws DAOException, BusinessException {
		testAgencia();
		testCliente();
		testCadastroConta();
		testOperacoesConta();
	}
	
	private static void testOperacoesConta() throws DAOException, BusinessException {
		ContaDAOImpl contaDAO = new ContaDAOImpl();
		
		Conta conta1 = contaDAO.findById(1L);
		System.out.println(conta1.toString());
		contaDAO.sacar(conta1, 20.0);
		System.out.println(conta1.toString());
		
		Conta conta2 = contaDAO.findById(2L);
		System.out.println(conta2.toString());
		contaDAO.depositar(conta2, 150.0);
		System.out.println(conta2.toString());
		
		contaDAO.transferir(conta1, conta2, 70.0);
		
		System.out.println(conta1.toString());
		System.out.println(conta2.toString());
	}

	private static void testCadastroConta() throws DAOException {
		AgenciaDAOImpl agenciaDAO = new AgenciaDAOImpl();
		ClienteDAOImpl clienteDAO = new ClienteDAOImpl();
		ContaDAOImpl contaDAO = new ContaDAOImpl();
		
		Agencia agencia1 = agenciaDAO.findById(1L);
		
		Cliente cliente1 = clienteDAO.findById(1L);
		List<Cliente> clientes1 = new ArrayList<>();
		clientes1.add(cliente1);
		
		Corrente corrente = new Corrente();
		corrente.setNumero("10000");
		corrente.setSaldo(300.50);
		corrente.setLimiteChequeEspecial(200.0);
		corrente.setAgencia(agencia1);
		corrente.setClientes(clientes1);
		
		contaDAO.addEntity(corrente);
		
		for (Corrente c : contaDAO.findAllContasCorrente()) {
			System.out.println(c.toString());
		}
		
		Agencia agencia2 = agenciaDAO.findById(2L);
		
		Cliente cliente2 = clienteDAO.findById(2L);
		List<Cliente> clientes2 = new ArrayList<>();
		clientes2.add(cliente2);
		
		Poupanca poupanca = new Poupanca();
		poupanca.setNumero("20000");
		poupanca.setRendimento(5000.0);
		poupanca.setSaldo(50000.0);
		poupanca.setAgencia(agencia2);
		poupanca.setClientes(clientes2);
		
		contaDAO.addEntity(poupanca);
		
		for (Poupanca p : contaDAO.findAllPoupancas()) {
			System.out.println(p.toString());
		}
	}

	private static void testCliente() throws DAOException {
		ClienteDAOImpl clienteDAO = new ClienteDAOImpl();
		
		Cliente cliente1 = new Cliente();
		cliente1.setNome("Joao Benedito Souza");
		cliente1.setSexo(Cliente.Sexo.MASCULINO);
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse("10/05/1985");
			cliente1.setDataNascimento(date);
		} catch (ParseException e) {}
		cliente1.setCpf("123456789");
		
		Cliente cliente2 = new Cliente();
		cliente2.setNome("Ana Maria");
		cliente2.setSexo(Cliente.Sexo.FEMININO);
		try {
			Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse("25/09/1990");
			cliente2.setDataNascimento(date2);
		} catch (ParseException e) {}
		cliente2.setCpf("568987655");
		
		clienteDAO.addEntity(cliente1);
		clienteDAO.addEntity(cliente2);
		
		for (Cliente c : clienteDAO.findAll()) {
			System.out.println(c.toString());
		}
	}

	private static void testAgencia() throws DAOException {
		AgenciaDAOImpl agenciaDAO = new AgenciaDAOImpl();
		
		Agencia agencia1 = new Agencia();
		agencia1.setNome("A1");
		agencia1.setNumero("0001");
		
		Agencia agencia2 = new Agencia();
		agencia2.setNome("A2");
		agencia2.setNumero("0002");
		
		agenciaDAO.addEntity(agencia1);
		agenciaDAO.addEntity(agencia2);
		
		for (Agencia a : agenciaDAO.findAll()) {
			System.out.println(a.toString());
		}
		
		agencia1.setNome("A1 updated");
		agenciaDAO.updateEntity(agencia1);
		
		System.out.println(agenciaDAO.findById(1L).toString());
		
		/*agenciaDAO.removeEntity(agencia1);
		
		for (Agencia a : agenciaDAO.findAll()) {
			System.out.println(a.toString());
		}*/
	}
	
}