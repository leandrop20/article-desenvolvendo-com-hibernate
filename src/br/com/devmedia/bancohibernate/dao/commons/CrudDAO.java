package br.com.devmedia.bancohibernate.dao.commons;

import java.io.Serializable;
import java.util.List;

import br.com.devmedia.bancohibernate.exception.DAOException;

public interface CrudDAO<T> {

	public List<T> findAll() throws DAOException;
	public T findById(Serializable id) throws DAOException;
	public void addEntity(T entity) throws DAOException;
	public void updateEntity(T entity) throws DAOException;
	public void removeEntity(T entity) throws DAOException;
	
}