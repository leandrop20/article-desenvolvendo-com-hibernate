package br.com.devmedia.bancohibernate.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.bancohibernate.dao.ClienteDAO;
import br.com.devmedia.bancohibernate.dao.commons.CrudDAOJPA;
import br.com.devmedia.bancohibernate.exception.DAOException;
import br.com.devmedia.bancohibernate.model.Cliente;

public class ClienteDAOImpl extends CrudDAOJPA<Cliente> implements ClienteDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> findAllClientesDividaChequeEspecial() throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "select c.clientes from Corrente c " + "where c.saldo < 0";
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			em.close();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> findAllClientesVips() throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "select p.clientes from Poupanca p " + "where (p.saldo + p.saldo*p.rendimento) > 100000";
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			em.close();
		}
	}

}