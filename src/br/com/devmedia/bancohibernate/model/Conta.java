package br.com.devmedia.bancohibernate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "contas")
public abstract class Conta {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long codigo;

	@Column(name = "numero")
	private String numero;

	@Column(name = "saldo")
	private Double saldo;

	@ManyToOne
	@JoinColumn(name = "id_conta")
	private Agencia agencia;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "contas_clientes", 
			joinColumns = @JoinColumn(name = "id_conta"), 
			inverseJoinColumns = @JoinColumn(name = "id_cliente"))
	private List<Cliente> clientes;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public abstract Double getSaldoTotal();

	public void sacar(Double valor) {
		this.saldo -= valor;
	}

	public void depositar(Double valor) {
		this.saldo += valor;
	}

	public void transferir(Conta contaDestino, Double valor) {
		this.saldo -= valor;
		contaDestino.setSaldo(contaDestino.getSaldo() + valor);
	}

	@Override
	public String toString() {
		return "Conta [codigo=" + codigo + ", numero=" + numero + ", saldo=" + saldo + ", agencia=" + agencia
				+ ", clientes=" + clientes + "]";
	}

}