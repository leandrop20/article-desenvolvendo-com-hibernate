package br.com.devmedia.bancohibernate.exception;

public class DAOException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DAOException(Throwable cause) {
		super(cause);
	}

}