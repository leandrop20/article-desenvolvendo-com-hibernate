package br.com.devmedia.bancohibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "correntes")
@PrimaryKeyJoinColumn(name = "id_conta")
public class Corrente extends Conta {

	@Override
	public String toString() {
		return "Corrente [limiteChequeEspecial=" + limiteChequeEspecial + "]";
	}

	@Column(name = "limite_cheque_especial")
	private Double limiteChequeEspecial;

	public Double getLimiteChequeEspecial() {
		return limiteChequeEspecial;
	}

	public void setLimiteChequeEspecial(Double limiteChequeEspecial) {
		this.limiteChequeEspecial = limiteChequeEspecial;
	}

	@Override
	public Double getSaldoTotal() {
		return getSaldo() + limiteChequeEspecial;
	}

}