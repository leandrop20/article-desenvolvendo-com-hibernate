package br.com.devmedia.bancohibernate.dao;

import java.util.List;

import br.com.devmedia.bancohibernate.dao.commons.CrudDAO;
import br.com.devmedia.bancohibernate.exception.BusinessException;
import br.com.devmedia.bancohibernate.exception.DAOException;
import br.com.devmedia.bancohibernate.model.Cliente;
import br.com.devmedia.bancohibernate.model.Conta;
import br.com.devmedia.bancohibernate.model.Corrente;
import br.com.devmedia.bancohibernate.model.Poupanca;

public interface ContaDAO extends CrudDAO<Conta> {

	public List<Corrente> findAllContasCorrente() throws DAOException;
	public List<Poupanca> findAllPoupancas() throws DAOException;
	public void sacar(Conta c, Double valor) throws DAOException, BusinessException;
	public void depositar(Conta c, Double valor) throws DAOException;
	public void transferir(Conta contaOrigem, Conta contaDestino, Double valor) 
			throws DAOException, BusinessException;
	public List<Conta> findAllContasByCliente(Cliente cliente) throws DAOException;
	
}