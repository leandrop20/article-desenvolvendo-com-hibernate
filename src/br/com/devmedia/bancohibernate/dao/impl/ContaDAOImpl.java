package br.com.devmedia.bancohibernate.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.bancohibernate.dao.ContaDAO;
import br.com.devmedia.bancohibernate.dao.commons.CrudDAOJPA;
import br.com.devmedia.bancohibernate.exception.BusinessException;
import br.com.devmedia.bancohibernate.exception.DAOException;
import br.com.devmedia.bancohibernate.model.Cliente;
import br.com.devmedia.bancohibernate.model.Conta;
import br.com.devmedia.bancohibernate.model.Corrente;
import br.com.devmedia.bancohibernate.model.Poupanca;

public class ContaDAOImpl extends CrudDAOJPA<Conta> implements ContaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Corrente> findAllContasCorrente() throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "from Corrente c";
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Poupanca> findAllPoupancas() throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "from Poupanca p";
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void sacar(Conta c, Double valor) throws DAOException, BusinessException {
		c.sacar(valor);
		this.updateEntity(c);
	}

	@Override
	public void depositar(Conta c, Double valor) throws DAOException {
		c.depositar(valor);
		this.updateEntity(c);
	}

	@Override
	public void transferir(Conta contaOrigem, Conta contaDestino, Double valor) 
			throws DAOException, BusinessException {
		contaOrigem.transferir(contaDestino, valor);
		
		EntityManager em = null;
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			
			contaOrigem = em.merge(contaOrigem);
			em.persist(contaOrigem);
			
			contaDestino = em.merge(contaDestino);
			em.persist(contaDestino);
			
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Conta> findAllContasByCliente(Cliente cliente) throws DAOException {
		EntityManager em = factory.createEntityManager();
		try {
			String q = "select c from Conta c " + "inner join fetch c.clientes cl " + "where cl.codigo = " +
					cliente.getCodigo();
			return em.createQuery(q).getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}