package br.com.devmedia.bancohibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "poupancas")
@PrimaryKeyJoinColumn(name = "id_conta")
public class Poupanca extends Conta {

	@Column(name = "rendimento")
	private Double rendimento;

	public Double getRendimento() {
		return rendimento;
	}

	public void setRendimento(Double rendimento) {
		this.rendimento = rendimento;
	}

	@Override
	public Double getSaldoTotal() {
		return getSaldo() * (1 + rendimento);
	}

	@Override
	public String toString() {
		return "Poupanca [rendimento=" + rendimento + "]";
	}

}